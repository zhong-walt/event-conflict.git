package com.loveyoung.eventconflict;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ViewConfiguration;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;


/**
 * Author:created  By Walt-zhong at 2021/9/15 11:47
 * e-Mail:2511255880@qq.com
 */
public class MyViewPager extends ViewPager {
    private static final String TAG = "MyViewPager";
    private float mLastX;
    private float mLastY;
    private boolean isIntercept;
    private int mTouchSlop;//这个变量用来衡量手指在屏幕上的最小距离，判断是否为MOVE
    private float initX;

    public MyViewPager(@NonNull Context context) {
        this(context,null);
    }

    public MyViewPager(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        mTouchSlop = viewConfiguration.getScaledPagingTouchSlop();
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        Log.e("zhongxj:","MotionEvent: " +ev.toString());
        //在子view中进行冲突处理.这时需要子view实现dispatchTouchEvent方法
//        if(ev.getAction() == MotionEvent.ACTION_DOWN){
//            super.onInterceptTouchEvent(ev);
//            return false;
//        }
//        return true;

        //在父view中进行冲突处理，不需要子view实现dispatchTouchEvent方法
        float x = ev.getX();
        float y = ev.getY();
        switch (ev.getAction()){
            case MotionEvent.ACTION_DOWN:
                super.onInterceptTouchEvent(ev);
                return false;
            case MotionEvent.ACTION_MOVE:
                float xDiff = Math.abs(x-mLastX);
                float yDiff = Math.abs(y-mLastY);

                if(xDiff > yDiff){
                    isIntercept = true;
                    break;
                }

                if(yDiff > xDiff){
                    isIntercept = false;
                    break;
                }
              //  break;

        }
        Log.e("zhongxj:","isIntercept: " +isIntercept);
        mLastX = x;
        mLastY = y;

        return isIntercept;
    }

}